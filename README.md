# Ejercicio 4

Dado un vector de 10 enteros inicializado con datos al definirlo, programe una función que
permita encontrar la posición del valor máximo. Usarla para informar cuál es el máximo y en
que posición del vector se encuentra. 